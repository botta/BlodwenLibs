module Finite

import Fin
import Vect
import Exists

interface Finite t where
  card   : Nat
  to     : t -> Fin card
  from   : Fin card -> t
  -- toFrom : (k : Fin card) -> to (from k) = k
  -- fromTo : (x : t) -> from (to x) = x

interface (Finite t) => Lala t where

interface Decidable t where
  em : Either t (Not t)

interface AllDecidable (t : Type) (p : t -> Type) where
  aem : (x : t) -> Either (p x) (Not (p x))

implementation (AllDecidable a p) => AllDecidable (Vect n a) (Any p) where
  aem         Nil = Right contra where
    contra : Not (Any p Nil)
    contra = anyNilAbsurd
  aem (Cons x xs) = pm (aem x) where
    pm : Either (p x) (Not (p x)) -> Either (Any p (Cons x xs)) (Not (Any p (Cons x xs)))
    pm (Left   px) = Left (AnyHere px)
    pm (Right npx) = pm' (aem xs) where
      pm' : Either (Any p xs) (Not (Any p xs)) ->
            Either (Any p (Cons x xs)) (Not (Any p (Cons x xs)))
      pm' (Left   apxs) =  Left (AnyThere apxs)
      pm' (Right napxs) = Right (notnotnot npx napxs)
      
AnyExistsLemma : Any p xs -> Exists a p

ElemAnyLemma   : {a : Type} -> {p : a -> Type} -> {x : a} -> {n : Nat} -> {xs : Vect n a} ->
                 p x -> Elem x xs -> Any p xs

toVect : (Finite t) => Vect (Finite.card {t}) t
toVect = toVect from

implementation (Finite a, AllDecidable a p) => Decidable (Exists a p) where
  em = existsOrNotExists where
    n                 : Nat
    n                 = card {t = a}
    xs                : Vect (card {t = a}) a -- Vect n a
    xs                = toVect {t = a}
    helper            : Either (Any p xs) (Not (Any p xs)) -> Either (Exists a p) (Not (Exists a p))
    helper            = ?helperImpl
    anyOrNotAny       : Either (Any p xs) (Not (Any p xs))
    anyOrNotAny       = ?anyOrNotAnyImpl -- aem {p = Any p} xs 
    existsOrNotExists : Either (Exists a p) (Not (Exists a p))
    existsOrNotExists = ?existsOrNotExistsImpl -- helper anyOrNotAny


implementation Finite (Fin n) where
  card   = ?n
  to     = ?lala
  from   = ?lula
  -- toFrom = ?kika

{-

CardZ : (Finite t) =>  Type
CardZ {t} = Finite.card {t} = Z

CardNotZ : (Finite t) =>  Type
CardNotZ {t} = Not (CardZ {t})


implementation Finite (Fin n) where
  card   = ?n
  to     = ?lala
  from   = ?lula

-}

-- implementation (Finite a, Finite b) => Finite (a, b) where
--   card   = ?cardImpl
--   to     = ?toImpl
--   from   = ?fromImpl

-- toVectComplete : (Finite t) => (a : t) -> Elem a toVect
-- toVectComplete (MkSigma _ iso) a = s3 where
--   s1  :  Elem (from iso (to iso a)) (toVect (from iso))
--   s1  =  toVectComplete (from iso) (to iso a)
--   s2  :  from iso (to iso a) = a
--   s2  =  fromTo iso a
--   s3  :  Elem a (toVect (from iso))
--   s3  =  replace {P = \ z => Elem z (toVect (from iso))} s2 s1
