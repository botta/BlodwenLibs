# BlodwenLibs

This repository is a first step towards porting
[IdrisLibs](https://gitlab.pik-potsdam.de/botta/IdrisLibs) to Blodwen.

For the time being, the focus is on refactoring Idris Libs components
that we would have liked to implement as Idris interfaces but we were
not able to do so, see for instance
(e.g. [IdrisLibs.Finite](https://gitlab.pik-potsdam.de/botta/IdrisLibs/tree/master/Finite)

Porting these components should allow to spot current bugs in Blodwen's
interfaces but also to provide early feedback on language design from
application-level software, see following notes.

## Abstract deduction schemes vs. named, representation dependent functions

In IdrisLibs, the canonical way of implementing generic properties, for
instance

```
If A and B are finite types, then (A,B) is a finite type (1)
```

has been through named generic functions. For instance, in
[IdrisLibs.Finite.Properties](https://gitlab.pik-potsdam.de/botta/IdrisLibs/blob/master/Finite/Properties.lidr)
we have implemented a `finitePair` function

```
finitePair : {A, B : Type} -> Finite A -> Finite B -> Finite (A, B)
```

In
[BlodwenLibs.Finite.Interface](https://gitlab.pik-potsdam.de/botta/BlodwenLibs/blob/master/Finite/Interface.blod)
we would like to implement (1) via an *abstract deduction scheme* (ADS)
like

```
implementation (Finite A, Finite B) => Finite (A,B) where
```

We expect ADSs to have three major advantages over generic
functions. First, they prevent the proliferation of names for
essentially the same propery: `finitePair`, `finiteTuple3`,
`finiteTuple4`, ..., `decidablePair`, `decidableTuple3`, etc.

If a theorem is relevant enough to be warranted a name, implementations
can be named:

```
implementation [finitePair] (Finite A, Finite B) => Finite (A,B) where
```

Second, we expect abstract deduction schemes to significantly improve
maintainability. In dependently typed programming, a large amount of
coding efforts is devoted to formulating and implementing program
*specifications*: sorting algorithms shall return sorted results, the
result of filtering a list shall only contain elements of the original
list, etc.

Implementing large libraries of properties on the basis of concrete
representations (e.g., of `Finite` as introduced through a `data`
declaration or a function definition) would make such libraries
vulnerable to even minor implementational changes. For instance,
redefining

```
Finite A = Sigma Nat (\ n => Iso A (Fin n))
```

as

```
data Finite : Type -> Type where
  MkFinite : (A : Type) -> (c : Nat) -> Iso A (Fin c) -> Finite A
```

would require trivial but potentially very expensive modifications to
the original implementation. In contrast, modifications of the
definitions of, e.g.

```
implementation Finite Bool
```

and

```
implementation Finite (Fin n)
```

would have no impact on the code that ensures the finiteness of `(Bool,
Fin n)`. 

Finally, we expect ADSs to take advantage of the support for automatic
name resolution built into the language (name overloading) to simplify
the implementation of proofs involving finite products.


## ADS, problem specification and natural deduction

Intuitionistic logic can be interpreted as a calculus of specifications
(problems, assignments, tasks), see [1]. If `A` represents a problem,
`Not A` represent the problem of deriving a contradiction from the
assumption that a solution of `A` is available. `(A,B)` represents the
problem of finding a solution for problem `A` and one for problem
`B`. And so on.

We can represent specifications in terms of functions that return values
of type `Type`, in terms of `data` declarations or in terms of
interfaces. In Blodwen, interfaces are syntactic sugar for ... Thus, a
natural question is when to use function and when to use data
declaration or interfaces.

We hope that porting the function-based specifications of IdrisLibs to
Blodwen interfaces will allow us to put forward some minimal guidelines.

One argument that speaks for interfaces is their capability to express
problems in a *natural* language. Consider, for example

```
If A is a finite type and P is a decidable property of A values, then Exists A P is decidable (2)
```

...




[1] Kolmogoroff A., Zur Deutung der intuitionistischen Logik

