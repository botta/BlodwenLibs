> %default total
> %auto_implicits off
> n : Nat
> n = 80000
> %freeze n
> ns : List Nat
> ns = [1..n]
> postulate lemma : (m : Nat) -> 2 * (sum [1..m]) = m * (S m)
> q : 2 * (sum ns) = n * (S n)
> q = lemma n
> main : IO ()
> main = do putStrLn ("n           = " ++ show n)
>           putStrLn ("sum ns      = " ++ show (sum ns))

