> import Data.Vect

> %default total
> %access public export

> interface Enumerated (t : Type) (len : Nat) | t where
>   values : Vect len t
>   toFin  : t -> Fin len
>   values_match_toFin : map toFin values = range

> using (t : Type, len : Nat)
>   implementation (Enumerated t len) => Eq t where
>     x == y = toFin x == toFin y

The program fails to type check with

- + Errors (1)
 `-- 4279.lidr line 12 col 19:
     Overlapping implementation: Eq () already defined

