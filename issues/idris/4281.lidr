> %default total
> %auto_implicits off

> X : Type
> H : Type
> fulfils : X -> H -> Bool

> LTE : H -> H -> Type
> LTE h1 h2 = (x : X) -> fulfils x h1 = True -> fulfils x h2 = True

> antisymmetricLTE : (h1 : H) -> (h2 : H) -> h1 `LTE` h2 -> h2 `LTE` h1 -> 
>                    (x : X) -> fulfils x h1 = fulfils x h2 
> antisymmetricLTE h1 h2 p q x with (fulfils x h1)
>   | True  = ?kika
>   | False = ?kiko

The program fails to type check with

- + Errors (1)
 `-- 4281.lidr line 13 col 31:
     Type mismatch between
             LTE h2 h1 (Type of q)
     and
             (x1 : X) ->
             (fulfils x1 h2 = True) -> fulfils x h1 = True (Expected type)
     
     Specifically:
             Type mismatch between
                     fulfils v1 h1
             and
                     fulfils x h1

but replacing

< antisymmetricLTE h1 h2 p q x with (fulfils x h1)

with

< antisymmetricLTE h1 h2 p q y with (fulfils y h1)

makes the program type check fine.
