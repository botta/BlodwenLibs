> %default total
> %auto_implicits off
> %access public export

> postulate Real    : Type 
> postulate plus    : Real -> Real -> Real
> postulate mult    : Real -> Real -> Real
> postulate fromNat : Nat -> Real

> implementation Num Real where
>   (+) = plus
>   (*) = mult
>   fromInteger = fromNat . fromIntegerNat

the program fails to type check with

- + Errors (1)
 `-- 4296.lidr line 10 col 17:
     Overlapping implementation: Num Integer already defined

but replacing

< implementation Num Real where

with

< implementation [Lala] Num Real where

makes the program type check.
