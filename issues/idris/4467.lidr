> module Main

> %default total
> %access public export
> %auto_implicits off

> J : Type -> Type -> Type
> J R X = (X -> R) -> X

> K : Type -> Type -> Type
> K R X = (X -> R) -> R

> overline : {X, R : Type} -> J R X -> K R X
> overline e p = p (e p)

> {-
> otimes : {X, R : Type} -> J R X -> (X -> J R (List X)) -> J R (List X)  
> otimes e f p = let x  = e (\ x' => overline (f x') (\ xs' => p (x' :: xs'))) in
>                let xs = f x (\ xs' => p (x :: xs')) in
>                x :: xs
> -}

> {-
> otimes : {X, R : Type} -> J R X -> (X -> J R (List X)) -> J R (List X)  
> otimes {X} {R} e f p = x :: xs where
>   x   =    e (\ x' => overline (f x') (\ xs' => p (x' :: xs')))
>   xs  =  f x (\ xs' => p (x :: xs'))
> -}

> x : {X, R : Type} -> J R X -> (X -> J R (List X)) -> (List X -> R) -> X  
> x {X} {R} e f p = e (\ x' => overline (f x') (\ xs' => p (x' :: xs')))

> xs : {X, R : Type} -> J R X -> (X -> J R (List X)) -> (List X -> R) -> List X  
> xs {X} {R} e f p = f (x e f p) (\ xs' => p ((x e f p) :: xs'))

> otimes : {X, R : Type} -> J R X -> (X -> J R (List X)) -> J R (List X)  
> otimes {X} {R} e f p = (x e f p) :: (xs e f p)

> partial
> bigotimes : {X, R : Type} -> List (List X -> J R X) -> J R (List X)
> bigotimes       []   =  \ p => []
> bigotimes (e :: es)  =  (e []) `otimes` (\x => bigotimes [\ xs => d (x :: xs) | d <- es])

> partial
> argsup : {X : Type} -> (xs : List X) -> J Int X
> argsup (x :: Nil) p = x
> argsup (x :: x' :: xs) p = if p x < p x' then argsup (x' :: xs) p else argsup (x :: xs) p

> partial
> e : List Int -> J Int Int
> e _ = argsup [0..100]

> p : List Int -> Int
> p = sum 

> partial
> main : IO ()
> main = do putStr ("bigotimes (replicate 3 e) p = "
>                   ++
>                   show (bigotimes (replicate 3 e) p) ++ "\n")

The program type checks and compiles (see Makefile). But increasing the
length of argsup to

< e _ = argsup [0..10]

or larger yields unacceptable exec times. An equivalent Haskell program

< module Main where

< type J r x = (x -> r) -> x

< type K r x = (x -> r) -> r

< overline :: J r x -> K r x
< overline e p = p (e p)

< otimes :: J r x -> (x -> J r [x]) -> J r [x]
< otimes e f p = x : xs where
<   x   =    e (\ x' -> overline (f x') (\ xs' -> p (x' : xs')))
<   xs  =  f x (\ xs' -> p (x : xs'))

< bigotimes ::  [[x] -> J r x] -> J r [x]
< bigotimes       []   =  \ p -> []
< bigotimes (e : es)   =  (e []) `otimes` (\x -> bigotimes [\ xs -> d (x : xs) | d <- es])

< argsup :: [x] -> J Int x
< argsup (x : []) p = x
< argsup (x : x' : xs) p = if p x < p x' then argsup (x' : xs) p else argsup (x : xs) p

< e :: [Int] -> J Int Int
< e _ = argsup [0..1]

< p :: [Int] -> Int
< p = sum

< main :: IO ()
< main = do putStr ("bigotimes (replicate 3 e) p = "
<                   ++
<                   show (bigotimes (replicate 3 e) p) ++ "\n")

runs in slightly more than quadratic time in the lengths of the argsup
argument for lengths up to 400.
